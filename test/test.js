var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')
var should = chai.should()

chai.use(chaiHttp) //Configurar chai con módulo http

// Definimos el conjunto de pruebas. Utilizamos funciones lambda (algo así como punteros a funciones)
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => { // Prueba 1: probar que google funciona
    chai.request('http://www.google.es')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})

describe('Tests de API usuarios', () => {
  it('Raíz OK', (done) => { // Prueba 2: probar que nuestra API funciona
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.mensaje.should.be.eql("Bienvenido a la API Tech-U del Red Ribon Army")
          done()
        })
  })
  it('Lista de usuarios', (done) => { // Prueba 3: probamos listar usuarios de nuestra API
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('email')
            res.body[i].should.have.property('password')
          }
          done()
        })
  })
})
