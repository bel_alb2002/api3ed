var express = require('express')
var bodyParser = require('body-parser')
var app = express()

var requestJson = require('request-json')

app.use(bodyParser.json())


//Definimos la URL raiz de nuestro acceso al MLAB
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/dbbancoace/collections"
var apiKey = "apiKey=xRg2mPSCU0QkfQh02S6pVtmtP7I1tOoY"
var clienteMlab
// Implementamos la v5 de la llamada a listar usuarios haciendo la llamada contra la base de datos
app.get('/apitechu/v5/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  })
})

// Implementamos la v5 de la llamada a ver la info de un usuario por id haciendo la llamada contra la base de datos
app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}' // Alternativa -> q={"id":' + id + '}&f={"nombre":1, "apellidos":1, "_id":0}''
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        var datos = {}
        datos.nombre = body[0].first_name
        datos.apellidos = body[0].last_name
        res.send(datos)
        // Alernativa -> res.send(body[0])
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

// Mi función de login contra base de datos vía get cogiendo los parámetros de la URL
app.get('/apitechu/v5/login/:email/:password', function(req, res){ // Inicio de sesión de un usuario
var email = req.params.email
var password = req.params.password
var query = 'q={"email":"' + email + '","password":"' + password + '"}'
clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) {
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
      var cambio = '{"$set":{"logged":true}}'
      clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
        var datos = {}
        datos.login = "ok"
        datos.id = body[0].id
        datos.nombre = body[0].first_name
        datos.apellidos = body[0].last_name
        res.send(datos)
      })
    }
    else {
      res.status(404).send('Usuario no encontrado')
    }
  }
})
})

// // Función de login de Ángel vía post cogiendo los parámetros de las cabeceras
// app.post('/apitechu/v5/login', function(req, res) {
//    var email = req.headers.email
//    var password = req.headers.password
//    var query = 'q={"email":"' + email + '","password":"' + password + '"}'
//    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
//    clienteMlab.get('', function(err, resM, body) {
//      if (!err) {
//        if (body.length == 1) {
//         res.send({"login":"ok", "id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
//        }
//        else {
//          res.status(404).send('Usuario no encontrado')
//        }
//      }
//    })
// })

// Mi función de login contra base de datos vía post cogiendo los parámetros del body
app.post('/apitechu/v5/login', function(req, res){ // Inicio de sesión de un usuario
var user = req.body
var email = user.email
var password = user.password
var query = 'q={"email":"' + email + '","password":"' + password + '"}'
clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) {
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
      var cambio = '{"$set":{"logged":true}}'
      clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
        var datos = {}
        datos.login = "ok"
        datos.id = body[0].id
        datos.nombre = body[0].first_name
        datos.apellidos = body[0].last_name
        res.send(datos)
     })
    }
    else {
      res.status(404).send('Usuario no encontrado')
    }
  }
})
})

// Mi función de logout contra base de datos vía post cogiendo los parámetros del body
app.post('/apitechu/v5/logout', function(req, res){
var body = req.body
var id = body.id
var query = 'q={"id":' + id + ', "logged":true}'
clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // Estaba logado
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
      var cambio = '{"$set":{"logged":false}}'
      clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
        var datos = {}
        datos.logout = "ok"
        datos.id = id
        res.send(datos)
     })
    }
    else {
      res.status(200).send('Usuario no logado en el sistema.')
    }
  }
})
})

// Dado un número de cuenta, devolver los movimientos pero atacando a la base de datos
app.get('/apitechu/v5/movimientos/:iban', function(req, res){
var iban = req.params.iban
var query = 'q={"iban":"' + iban + '"}'
var filter = 'f={"movimientos":1,"_id":0}'

clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // Hay cuentas
      res.send(body)
    }
    else {
      res.status(404).send('No existe ningún cliente con ese número de cuenta.')
    }
  }
})
})

// Devuelve todas las cuentas de un cliente dado pero atacando la base de datos
app.get('/apitechu/v5/cuentas/:idcliente', function(req, res){
var idcliente = req.params.idcliente
var query = 'q={"idcliente":' + idcliente + '}'
var filter = 'f={"iban":1,"_id":0}'
clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // Hay clientes
      res.send(body)
    }
    else {
      res.status(404).send('No existe ese cliente o no tiene cuentas.')
    }
  }
})
})

var port = process.env.PORT || 3000
var fs = require('fs')

console.log("Bienvenido a la API del curso Tech U Practitioner")

app.listen(port)

console.log("API escuchando en el puerto " + port)

var usuarios = require('./usuarios.json')
var users = require('./users.json')
var cuentas = require('./cuentas.json')

app.get('/apitechu/v1', function(req, res){
//console.log(req)
res.send({"mensaje":"Bienvenido a la API Tech-U del Red Ribon Army"})
})

app.get('/apitechu/v1/usuarios', function(req, res){ //Listar los usuario
res.send(usuarios)
})

app.post('/apitechu/v1/usuarios', function(req, res){ //Dar de alta un usuario nuevo. Paso de parámetros por headers
var nuevo = {"first_name":req.headers.first_name,
             "country":req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf-8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }
    })

  res.send("Alta efectuada correctamente")
})

app.delete('/apitechu/v1/usuarios/:id', function(req, res){ // Borrar un usuario
  usuarios.splice(req.params.id-1, 1)
  res.send("Usuario borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res){ // Prueba de paso d parámetros en la URL
  console.log("Parámetros:")
  console.log(req.params)
  console.log("Query Strings:")
  console.log(req.query)
  console.log("Headers:")
  console.log(req.headers)
  console.log("Body:")
  console.log(req.body)
})

app.post('/apitechu/v2/usuarios', function(req, res){ // V2 de insertar un usuario
  var nuevo = req.body
  usuarios.push(nuevo)
  console.log("nuevo = " + nuevo)
  console.log("email = " + nuevo.email)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf-8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado según API v2")
    }
    })

  res.send("Alta efectuada correctamente en API v2")
})

app.post('/apitechu/v1/login', function(req, res){ // Inicio de sesión de un usuario
// var nuevo = {"email":req.headers.email,
//              "password":req.headers.password
//            }
var email = req.headers.email
var password = req.headers.password
//console.log(req.headers)
  // Buscamos si el usuario existe en nuestro fichero
  var found = false
  var id = 0
  for (var i = 0; i < users.length; i++) {
    if (users[i].email == email && users[i].password == password)
    {
      found = true
      id = users[i].id
      users[i].logged = true
      console.log("Usuario " + id + " encontrado")
      console.log(users[i])
      break
    }
  }
  if (found) {
    res.send({"encontrado":"si","id":id})
  }
  else {
    res.send({"encontrado":"no"})
  }
})

app.post('/apitechu/v1/logout', function(req, res) { // Finalización de sesión de un usuario
  var id = req.headers.id
  var found = false
  for (var i = 0; i < users.length; i++) {
    if (users[i].id == id && users[i].logged == true)
    {
      found = true
      console.log("Usuario " + id + " encontrado y deslogueado")
      console.log(users[i])
      break
    }
  }
  if (found) {
    res.send({"encontrado":"si","id":id,"logout":"si"})
  }
  else {
    res.send({"logout":"no","mensaje":"El usuario no había iniciado sesión"})
  }
})

// // Listar las cuentas de los clientes
// app.get('/apitechu/v1/cuentas', function(req, res){
// var iban = []
// for (var i = 0; i < cuentas.length; i++) {
//   iban.push(cuentas[i].iban)
// }
// res.send(iban)
// })

// Listar las cuentas de los clientes
app.get('/apitechu/v1/cuentas', function(req, res){
var listaiban = {}
var iban = []
for (var i = 0; i < cuentas.length; i++) {
  iban.push(cuentas[i].iban)
}
listaiban.iban=iban
res.send(listaiban)
})


// //Dado un número de cuenta, devolver los movimientos
// app.get('/apitechu/v1/movimientos/:iban', function(req, res){ //Listar los usuario
// var iban = req.params.iban
// var movimientos = []
// for (var i = 0; i < cuentas.length; i++) {
//   if (cuentas[i].iban == iban){
//     movimientos.push(cuentas[i].movimientos)
//   }
// }
// res.send(movimientos)
// })

// Dado un número de cuenta, devolver los movimientos
app.get('/apitechu/v1/movimientos/:iban', function(req, res){ //Listar los usuario
var iban = req.params.iban
var listamovimientos = {}
var movimientos = []
for (var i = 0; i < cuentas.length; i++) {
  if (cuentas[i].iban == iban){
    movimientos.push(cuentas[i].movimientos)
  }
}
listamovimientos.iban = iban
listamovimientos.movimientos=movimientos
res.send(listamovimientos)
})

// Devuelve todas las cuentas de un cliente dado
app.get('/apitechu/v1/cuentas/:idcliente', function(req, res){ //Listar los usuario
var idcliente = req.params.idcliente
var listadocuentas = {}
var suscuentas = []

for (var i = 0; i < cuentas.length; i++) {
  if (cuentas[i].idcliente == idcliente){
    suscuentas.push(cuentas[i].iban)
  }
}

listadocuentas.idcliente = idcliente
listadocuentas.iban=suscuentas
res.send(listadocuentas)
})
